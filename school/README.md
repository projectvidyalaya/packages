#### Paḷḷi (Suggested name)
Paḷḷi means school in Tamil.

This is aimed at schools with pupils at age range between 4 and 16. 

Most of these schools have a low end computer with a legacy hardware. 

So the Paḷḷi will be running LDM as desktop manager and a light weight Desktop Environment such as LXDE, XFCE or Mate as desktop environment. 